
#Akinpelu Taofik Opeyemi B00902238
#COM101 Assignment

import matplotlib.pyplot as plt

# Function readData
def StockData():
    readData = open("stockDB.txt", "r")
    stock = []
    for i in readData:
        stock.append(i.rstrip('\n').split(','))
    return stock
         

#List all Data in the stock and display 
# Total number of Stock base on Length
# Overall number of Stock
# Total value of stock
def displayAllStock():
    readData = StockData()

    print()
    print("*****************************************************************************************************\n")
    totalStock = 0
    box50,box100, box200 = 0, 0, 0

    totalValue = 0
    tableArr = [["MATERIAL", "TYPE", "LENGTH", "STOCK OF 50", "STOCK OF 100", "STOCK OF 200", "AMOUNT(£)", "DISCOUNT"]]

    for item in readData:
        tableArr.append([item[0].upper(), item[1].upper(), item[2], item[3], item[4], item[5], item[6], item[7].upper()])
        box50 += int(item[3])
        box100 += int(item[4])
        box200 += int(item[5])
        totalValue += (float(item[6]) * (box50 + box100 +box200))
    totalStock = box50+box100+box200
            
    table(tableArr)
    print("*****************************************************************************************************\n")
    print("-> 50 Screw in Box Total Stock: "+str(box50)+" Unit"  )
    print("-> 100 Screws in Box Total Stock : "+str(box100)+" Unit"  )
    print("-> 200 Screw in Box Total Stock: "+str(box200)+" Unit"  )
    print("=> Total Stock Overall: "+str(totalStock)+" Unit"  )
    print("=> Total Value for all Stock: £",format(totalValue, ',.2f'))

#Function to Genertate Table Display
#param @rows array[]
def table(rows = []):
    maxItemlength = [
        (max([len(str(row[i])) for row in rows]) + 3)
        for i in range(len(rows[0]))
    ]
    itemFormat = "".join(["{:>" + str(maxLength) + "}" for maxLength in maxItemlength])
    for item in rows:
        print(itemFormat.format(*item))
    return


# Display quantity of stock by screw length
def screwLengthDisplay():
    readData = StockData()
    stockFor20,stockFor40,stockFor60 = 0, 0, 0
    
    print()
    print("*****************************************************************************************************\n")
    for item in readData:
        if (int(item[2]) == 20):
            stockFor20 += (int(item[3])+int(item[4])+int(item[5]))
        if (int(item[2]) == 40):
            stockFor40 += (int(item[3])+int(item[4])+int(item[5]))
        if (int(item[2]) == 60):
            stockFor60 += (int(item[3])+int(item[4])+int(item[5]))
            
    print("List of Total Stock available for Screw Length \n")
    print("Total Stock of 20MM: ", str(stockFor20), "")
    print("Total Stock of 40MM: ", str(stockFor40), "")
    print("Total Stock of 60MM: ", str(stockFor60), "")
    print("*****************************************************************************************************\n")
    
# Display a record of stock base on User request (length and Box)
# Display Quantity and Value of Stock
def suppliedScrewLengthData():
    readData = StockData()
    screwLength = input("Enter the screw length details (20, 40, 60): ")
    boxOf = input("Enter the Box quantity (50, 100, 200): ")
        
    print()
    print("*****************************************************************************************************\n")
    tableArr = [["MATERIAL", "TYPE", "LENGTH", "STOCK OF 50", "STOCK OF 100", "STOCK OF 200", "AMOUNT(£)", "DISCOUNT"]]
    totalStock = 0
    totalValue = 0
    for item in readData:
        if(item[2] == screwLength):
            tableArr.append([item[0].upper(), item[1].upper(), item[2], item[3], item[4], item[5], item[6], item[7].upper()])
            if(boxOf == "50"):
                totalStock +=int(item[3])
            if(boxOf == "100"):
                totalStock +=int(item[4])
            if(boxOf == "200"):
                totalStock +=int(item[5])
            totalValue +=float(item[6]) * totalStock

    table(tableArr)
    print("*****************************************************************************************************\n")
    print("Stock Total available for "+screwLength+"MM & "+boxOf+" screws in a Box is: "+str(totalStock)+" Unit"  )
    print("Value Total for "+screwLength+"MM: £",format(totalValue, ',.2f'))

# Function to display Record base on User Request on Material, length and screw Head
# increase and decrease the stock of material in respect to box size
# confirm if the level of value when to decrease
# show option to decrease level with partial quantity of stock if value to decrease is higher than stock
def doIncreaseDecreaseOfStock():
    readData = StockData()
    material = input("Enter name in material category (Brass, Steel, ...): ").upper().strip("\t").rstrip().lstrip()
    materialType = input("Enter the Head Type (Star, Slot, Pozidriv): ").upper().strip("\t").rstrip().lstrip()
    materialLength = input("Enter screw length (20, 40, 60): ").strip("\t").rstrip().lstrip()
    
    count = 0
    tableArr = [["MATERIAL", "TYPE", "LENGTH", "STOCK OF 50", "STOCK OF 100", "STOCK OF 200", "AMOUNT(£)", "DISCOUNT"]]
    for item in readData:
        if(item[0].upper() == material and item[1].upper() == materialType and item[2] == materialLength ):
            tableArr.append([item[0].upper(), item[1].upper(), item[2], item[3], item[4], item[5], item[6], item[7].upper()])
            count +=1
    
    if(count> 0):
        print()
        print("*****************************************************************************************************\n")
        table(tableArr)
        print("*****************************************************************************************************\n")
        print()
        
        print("Choose an option for the next action:\n")
        print("a. Increase the stock Level")
        print("b. Decrease the stock Level")
        print("any key to go main menu")
        
        newInput = input("Please enter value: ").strip("\t").rstrip().lstrip()
        
        match newInput.lower():
            case "a":
                increaseStock(material, materialType, materialLength)
            case "b":
                __decreaseStock(material, materialType, materialLength)
            case _:
                print()
    else:
        print()
        print("No Stock Available for Material: "+material+", Head Type: "+materialType+" & Screw Length: "+materialLength)
        print()
            

#Increase the stock Level 
# param @meterial
# param @meterialType
# param @meterialLength
#return          
def increaseStock(material, materialType, materialLength):
    readData = StockData()
    print("*****************************************************************************************************\n")
    print("Increase the stock of Material: "+material+", Head Type: "+materialType+", Screw Length: "+materialLength)
    increaseBox = input("Enter which box value you wish to increase its stock. i.e (50, 100, 200): ").strip('\t').lstrip().rstrip()
    print()
    increaseValue = int(input("Type in value to increase the stock with: "))
    
    presentLineNo, newStock, update = -1, '', True
    
    for item in readData:
        if(item[0].upper() == material and item[1].upper() == materialType and item[2] == materialLength ):
            presentLineNo = readData.index(item)
            match increaseBox:
                case "50":
                    newStock = material.lower()+","+materialType.lower()+","+materialLength+","+str(int(item[3]) + increaseValue)+","+item[4]+","+item[5]+","+item[6]+","+item[7]
                case "100":
                    newStock = material.lower()+","+materialType.lower()+","+materialLength+","+item[3]+","+str(int(item[4]) + increaseValue)+","+item[5]+","+item[6]+","+item[7]
                case "200":
                    newStock = material.lower()+","+materialType.lower()+","+materialLength+","+item[3]+","+item[4]+","+str(int(item[5]) + increaseValue)+","+item[6]+","+item[7]
                case _:
                    update = False

    if(update == True):
        updateStock(presentLineNo, newStock)
        print()
        print("*****************************************************************************************************\n")
        print("Stock has been updated successfully")
    else:
        print()
        print("*****************************************************************************************************\n")
        print("Stock could not be updated")

    readData = StockData()
    tableArr = [["MATERIAL", "TYPE", "LENGTH", "STOCK OF 50", "STOCK OF 100", "STOCK OF 200", "AMOUNT(£)", "DISCOUNT"]]
    for item in readData:
        if(item[0].upper() == material and item[1].upper() == materialType and item[2] == materialLength ):
            tableArr.append([item[0].upper(), item[1].upper(), item[2], item[3], item[4], item[5], item[6], item[7].upper()])
    print()
    print("New stock of Material: "+material+", Head Type: "+materialType+", Length: "+materialLength)
    print()
    table(tableArr)
    print("*****************************************************************************************************\n")
    print()
    print("Available stock record")
    displayAllStock()


#decrease the stock Level
# param @meterial
# param @meterialType
# param @meterialLength
#return
def __decreaseStock(material, materialType, materialLength):
    readData = StockData()
    print()
    print("*****************************************************************************************************\n")
    print("Decreasing the stock of Material: "+material+", Head Type: "+materialType+", Screw Length: "+materialLength)
    decreaseBox = input("Enter which box value you wish to decrease its stock. i.e (50, 100, 200): ").strip('\t').lstrip().rstrip()
    print()
    decreaseValue = int(input("Type in value to decrease the stock with: "))

    presentLineNo,newStock,update = -1, '', True

    for item in readData:
        if(item[0].upper() == material and item[1].upper() == materialType and item[2] == materialLength ):
            presentLineNo = readData.index(item)
            match decreaseBox:
                case "50": #do decrease for box of  50
                    if(int(item[3]) < decreaseValue):
                        newStock = material.lower()+","+materialType.lower()+","+materialLength+","+str(0)+","+item[4]+","+item[5]+","+item[6]+","+item[7]
                        if(doStockCheck(presentLineNo, newStock, item[3]) == True):
                            update = True
                        else:
                            update = False
                    else:
                        newStock = material.lower()+","+materialType.lower()+","+materialLength+","+str(int(item[3]) - decreaseValue)+","+item[4]+","+item[5]+","+item[6]+","+item[7]

                case "100": #do decrease for box of  100
                    if(int(item[4]) < decreaseValue):
                        newStock = material.lower()+","+materialType.lower()+","+materialLength+","+item[3]+","+str(0)+","+item[5]+","+item[6]+","+item[7]
                        if(doStockCheck(presentLineNo, newStock, item[4]) == True):
                            update = True
                        else:
                            update = False
                    else:
                        newStock = material.lower()+","+materialType.lower()+","+materialLength+","+item[3]+","+str(int(item[4]) - decreaseValue)+","+item[5]+","+item[6]+","+item[7]

                case "200": #do decrease for box of  200
                    if(int(item[5]) < decreaseValue):
                        newStock = material.lower()+","+materialType.lower()+","+materialLength+","+item[3]+","+item[4]+","+str(0)+","+item[6]+","+item[7]

                        if(doStockCheck(presentLineNo, newStock, item[5]) == True):
                            update = True
                        else:
                            update = False
                    else:
                        newStock = material.lower()+","+materialType.lower()+","+materialLength+","+item[3]+","+item[4]+","+str(int(item[5]) - decreaseValue)+","+item[6]+","+item[7]

                case _: #do decrease when invalid box value is provided
                    update = False

    if(update == True):
        updateStock(presentLineNo, newStock)
        print()
        print("*****************************************************************************************************\n")
        print("Stock Updated successfully")
    else:
        print()
        print("*****************************************************************************************************\n")
        print("No stock updated")


    readData = StockData()
    tableArr = [["MATERIAL", "TYPE", "LENGTH", "STOCK OF 50", "STOCK OF 100", "STOCK OF 200", "AMOUNT(£)", "DISCOUNT"]]
    for item in readData:
        if(item[0].upper() == material and item[1].upper() == materialType and item[2] == materialLength ):
            tableArr.append([item[0].upper(), item[1].upper(), item[2], item[3], item[4], item[5], item[6], item[7].upper()])
    print()
    print("New stock of Material: "+material+", Head Type: "+materialType+", Screw Length: "+materialLength)
    print()
    table(tableArr)
    print("*****************************************************************************************************\n")
    print()
    print("Available stock record")
    displayAllStock()


#Function to update Data in file
# param @lineIndex
# param @stocktoAdd
def updateStock(lineIndex, stocktoAdd):
    with open("stockDB.txt", "r") as f:
        data = f.readlines()
        data[lineIndex] = stocktoAdd+"\n"
    with open('stockDB.txt', 'w') as f:
        f.writelines(data)

#Function to request for partial decrement of stock
# param @lineIndex
# param @stocktoAdd
# param @availableValue
def doStockCheck(lineIndex, stocktoAdd, availableValue):
    print()
    print("*****************************************************************************************************\n")
    print("Stock Available ("+str(availableValue)+") is lesser than the quantity you supplied. ")
    goahead = input("To continue with partial stock sales/decrement of ("+str(availableValue)+"), input 1 or any value to cancel: ").strip('\t').lstrip().rstrip()
    print()

    if(goahead == '1'):
        updateStock(lineIndex, stocktoAdd)
        return True
    else:
        return False


def doDiscount():
    readData = StockData()
    lineIndex, highest = 0, 0

    for item in readData:
        total = int(item[3]) + int(item[4]) +int(item[5])
        #print("Total for index",readData.index(item), " => ", total)
        if(total > highest):
            highest = total
            #print("The higest index",readData.index(item), " => ", highest)
            lineIndex = readData.index(item)

    tableArr = [["MATERIAL", "TYPE", "LENGTH", "STOCK OF 50", "STOCK OF 100", "STOCK OF 200", "AMOUNT(£)", "DISCOUNT"]]
    tableArr.append([readData[lineIndex][0].upper(), readData[lineIndex][1].upper(), readData[lineIndex][2], readData[lineIndex][3], readData[lineIndex][4], readData[lineIndex][5], readData[lineIndex][6], readData[lineIndex][7].upper() ])

    print()
    print("*****************************************************************************************************\n")
    print("Below is the highest stock level")
    table(tableArr)
    print()

    disValue = input("Do you want to apply 10% discount to the highest stock level? (y/n): ").strip("\t").rstrip().lstrip()

    if(disValue.lower() == 'y' or disValue.lower() == 'yes'):
        stocktoAdd = readData[lineIndex][0].lower()+","+readData[lineIndex][1].lower()+","+readData[lineIndex][2]+","+readData[lineIndex][3]+","+readData[lineIndex][4]+","+readData[lineIndex][5]+","+readData[lineIndex][6]+", yes"
        updateStock(lineIndex, stocktoAdd)
    print()
    disContinue = input("Do you want to continue with stocks currently assigned discounts? (y/n): ").strip("\t").rstrip().lstrip()

    if(disContinue.lower() == 'n' or disContinue.lower() == 'no'):
        readData = StockData()
        for col in readData:
            if (readData.index(col) != lineIndex):
                if(col[7].lower().strip() == 'yes'):
                    stocktoAdd = col[0].lower()+","+col[1].lower()+","+col[2]+","+col[3]+","+col[4]+","+col[5]+","+col[6]+",no"
                    updateStock(readData.index(col), stocktoAdd)
        print()
        print("Stock information updated successfully")

    print()
    print("*****************************************************************************************************\n")
    displayAllStock()

    
# Show a bar chart of stock in respect to length
def plotBarChat():
    fig = plt.figure()
    readData = StockData()

    stockFor20, stockFor40,stockFor60 = 0, 0, 0
    
    for item in readData:
        if (int(item[2]) == 20):
            stockFor20 += (int(item[3])+int(item[4])+int(item[5]))
        if (int(item[2]) == 40):
            stockFor40 += (int(item[3])+int(item[4])+int(item[5]))
        if (int(item[2]) == 60):
            stockFor60 += (int(item[3])+int(item[4])+int(item[5]))
            
    ax = fig.add_axes([0.07,0.07,0.75,0.75])
    screwLength = ['20MM', '40MM', '60MM']
    screwLengthValues = [int(stockFor20),int(stockFor40),int(stockFor60)]
    ax.bar(screwLength,screwLengthValues, color=['black', 'red', 'green'])
    plt.show()


#Function to display Menu Options
def menu():
    print()
    print("*****************************************************************************************************")
    print("Welcome to Taofik Opeyemi AKinpelu (B00902238) Screw Stock System.")
    print()
    print("Provided below the task that can be initiate from the stock:")
    print("a. Display a List of all Screws available in stock.")
    print("b. Display a record containing total number of units in stock sorted by length category.")
    print("c. Generate list record of screws and their respective details length provided.")
    print("d. Query a particular screw category (defined by length, material and head type) is available and present options of (a) increasing stock level or (b) decreasing the stock level, by x unit of whatever box size they’ve selected due to a sale. In the case of a sale: (a) if the order exceeds the stock level the user should be informed that the order can only partially fulfilled and asked if they wish to continue with the order, (b) the total cost of the final order should be shown. You can assume that a user can only order one type of screw and box size at a time.")
    print("e. Create a discount feature where the user is told what screw category has the largest number of screws in stock and asked if they wish to place a 10% discount feature on that category to encourage sales. If another discount is currently running the user should be asked if they which continue with it or stop it. At most one category should be discounted at any one time.")
    print("f. Plot and display bar chart presenting data in respect to units existing for each length category in the stock available.")
    print()
    
#Initial while loop for Menu
__input = ''
while (__input != 0):
    menu()
    __input = input("Supply Task value to initiate task: ").strip("\t").rstrip().lstrip()
    match __input.lower():
        case "a":
            displayAllStock()
        case "b":
            screwLengthDisplay()
        case "c":
            suppliedScrewLengthData()
        case "d":
            doIncreaseDecreaseOfStock()
        case "e":
            doDiscount()
        case "f":
            plotBarChat()
        case _:
            print("Value entered is not valid")
            print()
            print("*****************************************************************************************************\n")
        
   
    
    
    
